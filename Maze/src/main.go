package main

import (
	"fmt"
	"maze"
)

func main() {
	m := maze.NewMaze(10, 10).Entrance(2, 3).Exit(9, 8).Rectangle().Perfect()
	fmt.Println(m)
}
