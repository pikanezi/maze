package maze

import "testing"

const (
	widthTest  = 200
	heightTest = 200
)

func expect(t *testing.T, description string, a, b interface{}) {
	if a != b {
		t.Fatalf("[%s] Failure! Expected %#v but got %#v\n", description, b, a)
	}
	t.Logf("[%s] Success! %#v\n", description, a)
}

func expectNot(t *testing.T, description string, a, b interface{}) {
	if a == b {
		t.Fatalf("[%s] Failure! %#v == %#v\n", description, a, b)
	}
	t.Logf("[%s] Success! %#v != %#v\n", description, a, b)
}

func TestCell(t *testing.T) {
	c := NewCell(0, 0)
	c.SetWallsDown(North, West)
	expect(t, "Number of Walls", CellNumberWalls, len(c.Walls))
}

func TestDoor(t *testing.T) {
	w := NewWall()
	expect(t, "Wall Down", WallStatusDown, w.Status)
}

func TestMaze(t *testing.T) {
	m := NewMaze(widthTest, heightTest).Rectangle().Perfect()
	expect(t, "Maze Height", heightTest, len(m.Cells))
	expect(t, "Maze Width", widthTest, len(m.Cells[0]))
	expect(t, "Shape", MazeRectangleShape, m.Shape)
	expectNot(t, "Entrance", m.EntranceCell, nil)
	expectNot(t, "Exit", m.ExitCell, nil)
}
