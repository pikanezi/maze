package maze

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

/*
	WALLS
*/

const (
	// WallStatusUp represents an wall up.
	WallStatusUp = "up"
	// WallStatusDown represents a wall down.
	WallStatusDown = "down"
)

// Wall represents a wall. It has a direction which is its direction compared to its cell.
type Wall struct {
	Status   string    `json:"status"`
	IsBorder bool      `json:"isBorder"`
}

// NewWall returns a new wall with the given direction.
func NewWall() *Wall { return &Wall{WallStatusDown, false} }

// Up set the wall up.
func (w *Wall) Up() { w.Status = WallStatusUp }

// Down set the wall down.
func (w *Wall) Down() { w.Status = WallStatusDown }

// IsUp returns whether the wall is up or not.
func (w *Wall) IsUp() bool { return w.Status == WallStatusUp }

// IsDown returns whether the wall is down or not.
func (w *Wall) IsDown() bool { return w.Status == WallStatusDown }

// String returns the representation of the door
func (w *Wall) String() string {
	return fmt.Sprintf(`Door{"status": "%s", "isBorder": "%v"`, w.Status, w.IsBorder)
}

/*
	CELLS
*/

const (
	// CellNumberWalls represents the number of walls a cell can have.
	CellNumberWalls = 4
)

type CellField [][]*Cell

// Unify unify each wall of the cells so each one of them are linked.
func (f CellField) Unify(maze *Maze) {
	for y := 0; y < len(f); y++ {
		for x := 0; x < len(f[y]); x++ {
			cell := f[y][x]
			if maze.CellEast(cell) != nil {
				cell.Walls[East] = maze.CellEast(cell).Walls[West]
			}
			if maze.CellWest(cell) != nil {
				cell.Walls[West] = maze.CellWest(cell).Walls[East]
			}
			if maze.CellNorth(cell) != nil {
				cell.Walls[North] = maze.CellNorth(cell).Walls[South]
			}
			if maze.CellSouth(cell) != nil {
				cell.Walls[South] = maze.CellSouth(cell).Walls[North]
			}
		}
	}
}

// Cell represents each cell of the maze.
type Cell struct {
	X       int                 `json:"x"`
	Y       int                 `json:"y"`
	Walls   map[Direction]*Wall `json:"walls"`
	Outside bool                `json:"outside"`
	Visited bool                `json:"visited"`
}

// NewCell returns a new cell at the position [x, y].
func NewCell(x, y int) *Cell {
	c := &Cell{X: x, Y: y}
	c.init()
	return c
}

func (c *Cell) init() {
	c.Walls = make(map[Direction]*Wall, CellNumberWalls)
	for _, direction := range EveryDirections {
		c.Walls[direction] = NewWall()
	}
}

// HasWallDown returns whether its wall at the given direction is down.
func (c *Cell) HasWallDown(direction Direction) bool { return c.Walls[direction].IsDown() }

// GetWall returns the wall at the given direction.
func (c *Cell) GetWall(direction Direction) *Wall { return c.Walls[direction] }

// SetWallsDown set the walls down at the given directions.
func (c *Cell) SetWallsDown(directions ...Direction) {
	for _, dir := range directions {
		c.Walls[dir].Down()
	}
}

// SetBorders set the walls down at the given directions and set the wall as a border.
func (c *Cell) SetBorders(directions ...Direction) {
	for _, dir := range directions {
		c.Walls[dir].Down()
		c.Walls[dir].IsBorder = true
	}
}

/*
	MAZE
*/

// Direction is a cardinal direction.
type Direction string

const (
	// West is west...
	West = Direction("west")
	// South is south...
	South = Direction("south")
	// East is east...
	East = Direction("east")
	// North is north...
	North = Direction("north")
	// DirectionInvalid is unused...
	DirectionInvalid = Direction("")
)

// RandomDirection returns a random direction.
func RandomDirection() Direction {
	return EveryDirections[rand.Intn(len(EveryDirections))]
}

var (
	// EveryDirections is an array of each direction.
	EveryDirections = []Direction{West, South, East, North}
)

const (
	// MazeRectangleShape is the Rectangle shape of a maze.
	MazeRectangleShape = "rectangle"
)

// Maze represents a maze.
type Maze struct {
	Width        int       `json:"width"`
	Height       int       `json:"height"`
	Cells        [][]*Cell `json:"cells"`
	Borders      []*Cell   `json:"borders"`
	BacktrackPath []*Cell   `json:"solutionPath"`
	EntranceCell *Cell     `json:"entrance"`
	ExitCell     *Cell     `json:"exit"`
	Shape        string    `json:"shape"`
}

// NewMaze returns a maze almost ready. After this functions, you need to call a Shape function (such as Rectangle()) and
// a maze creation function (such as Perfect()).
func NewMaze(width, height int) *Maze {
	cells := make([][]*Cell, height)
	for y := 0; y < height; y++ {
		cells[y] = make([]*Cell, width)
		for x := 0; x < width; x++ {
			cells[y][x] = NewCell(x, y)
		}
	}
	maze := &Maze{
		Width:        width,
		Height:       height,
		Cells:        cells,
		Borders:      make([]*Cell, 0),
		BacktrackPath: make([]*Cell, 0),
		EntranceCell: cells[0][0],
		ExitCell:     cells[height-1][width-1],
	}
	CellField(cells).Unify(maze)
	return maze
}

// TotalCells returns the total number of cells.
func (m *Maze) TotalCells() int { return m.Width * m.Height }

// CellNorth returns the cell to the north of the given cell.
func (m *Maze) CellNorth(cell *Cell) *Cell { return m.Cell(cell.X, cell.Y-1) }

// CellSouth returns the cell to the south of the given cell.
func (m *Maze) CellSouth(cell *Cell) *Cell { return m.Cell(cell.X, cell.Y+1) }

// CellWest returns the cell to the west of the given cell.
func (m *Maze) CellWest(cell *Cell) *Cell { return m.Cell(cell.X-1, cell.Y) }

// CellEast returns the cell to the east of the given cell.
func (m *Maze) CellEast(cell *Cell) *Cell { return m.Cell(cell.X+1, cell.Y) }

// Rectangle set the shape of the maze as a rectangle.
func (m *Maze) Rectangle() *Maze {
	m.Shape = MazeRectangleShape
	for y := 0; y < m.Height; y++ {
		for x := 0; x < m.Width; x++ {
			if x == 0 || y == 0 || x == m.Width-1 || y == m.Height-1 {
				cell := m.Cell(x, y)
				var directions []Direction
				if x == 0 {
					directions = append(directions, West)
				}
				if y == 0 {
					directions = append(directions, North)
				}
				if x == m.Width-1 {
					directions = append(directions, East)
				}
				if y == m.Height-1 {
					directions = append(directions, South)
				}
				cell.SetBorders(directions...)
				m.Borders = append(m.Borders, cell)
			}
		}
	}
	return m
}

// OutsideBorders check if the position [x, y] is outside of the boundaries of the maze.
func (m *Maze) OutsideBorders(x, y int) bool {
	if x >= m.Width || x < 0 || y >= m.Height || y < 0 {
		return true
	}
	return false
}

// Cell returns the cell at the position [x, y] or nil if it is outside of the boundaries of the maze.
func (m *Maze) Cell(x, y int) *Cell {
	if m.OutsideBorders(x, y) {
		return nil
	}
	return m.Cells[y][x]
}

// Entrance set the EntranceCell at the position [x, y]. Has no effects if the position is outside of the boundaries of the maze
func (m *Maze) Entrance(x, y int) *Maze {
	cell := m.Cell(x, y)
	if cell != nil {
		m.EntranceCell = cell
	}
	return m
}

// Exit set the ExitCell at the position [x, y]. Has no effects if the position is outside of the boundaries of the maze
func (m *Maze) Exit(x, y int) *Maze {
	cell := m.Cell(x, y)
	if cell != nil {
		m.ExitCell = cell
	}
	return m
}

// RandomUnvisitedNeighborOf returns a random neighbor unvisited cell of the given cell.
func (m *Maze) RandomUnvisitedNeighborOf(cell *Cell) *Cell {
	var neighbors []*Cell
	if neighbor := m.CellEast(cell); neighbor != nil && !neighbor.Visited {
		neighbors = append(neighbors, neighbor)
	}
	if neighbor := m.CellSouth(cell); neighbor != nil && !neighbor.Visited {
		neighbors = append(neighbors, neighbor)
	}
	if neighbor := m.CellWest(cell); neighbor != nil && !neighbor.Visited {
		neighbors = append(neighbors, neighbor)
	}
	if neighbor := m.CellNorth(cell); neighbor != nil && !neighbor.Visited {
		neighbors = append(neighbors, neighbor)
	}
	// Shuffle the slice
	for i := range neighbors {
		j := rand.Intn(i + 1)
		neighbors[i], neighbors[j] = neighbors[j], neighbors[i]
	}
	if len(neighbors) == 0 {
		return nil
	}
	return neighbors[rand.Intn(len(neighbors))]
}

func (m *Maze) WallBetween(a, b *Cell) *Wall {
	if m.CellEast(a) == b {
		return a.GetWall(East)
	}
	if m.CellNorth(a) == b {
		return a.GetWall(North)
	}
	if m.CellSouth(a) == b {
		return a.GetWall(South)
	}
	if m.CellWest(a) == b {
		return a.GetWall(West)
	}
	return nil
}

// Perfect executes a depth-first search to generate a perfect maze.
func (m *Maze) Perfect() *Maze {
	currentCell := m.ExitCell
	visitedCells := 1
	for visitedCells < m.TotalCells() {
		neighbor := m.RandomUnvisitedNeighborOf(currentCell)
		if neighbor != nil {
			m.BacktrackPath = append(m.BacktrackPath, neighbor)
			m.WallBetween(currentCell, neighbor).Up()
			currentCell = neighbor
			visitedCells++
		} else {
			var c *Cell
			c, m.BacktrackPath = m.BacktrackPath[len(m.BacktrackPath)-1], m.BacktrackPath[:len(m.BacktrackPath)-1]
			currentCell = c
		}
	}
	return m
}

// String returns the representation of the maze.
func (m *Maze) String() (s string) {
	if m.Shape == MazeRectangleShape {
		for y := 0; y < m.Height; y++ {
			cells := m.Cells[y]
			s = m.stringTop(s, cells)
			s = m.stringMiddle(s, cells)
			if y == m.Height-1 {
				s = m.stringBottom(s, cells)
			}
		}
	}
	return
}

func (m *Maze) stringBottom(s string, cells []*Cell) string {
	if m.Shape == MazeRectangleShape {
		for _, cell := range cells {
			if cell.HasWallDown(South) {
				s += "+--"
			} else {
				s += "+  "
			}
		}
		s += "+\r\n"
	}
	return s
}

func (m *Maze) stringMiddle(s string, cells []*Cell) string {
	if m.Shape == MazeRectangleShape {
		for i := 0; i < len(cells); i++ {
			cell := cells[i]
			if cell.HasWallDown(West) && cell != m.EntranceCell && cell != m.ExitCell {
				s += "|  "
			} else if cell != m.EntranceCell && cell != m.ExitCell {
				s += "   "
			}
			if cell == m.EntranceCell && cell.HasWallDown(West) {
				s += "|o "
			} else if cell == m.EntranceCell {
				s += " o "
			}
			if cell == m.ExitCell && cell.HasWallDown(West) {
				s += "| x"
			} else if cell == m.ExitCell {
				s += "  x"
			}
			if i == len(cells)-1 {
				if cell.HasWallDown(East) {
					s += "|\r\n"
				} else {
					s += " \r\n"
				}
			}
		}
	}
	return s
}

func (m *Maze) stringTop(s string, cells []*Cell) string {
	if m.Shape == MazeRectangleShape {
		for _, cell := range cells {
			if cell.HasWallDown(North) {
				s += "+--"
			} else {
				s += "+  "
			}
		}
		s += "+\r\n"
	}
	return s
}
